### Portfolio

* [Django, DRF, JWT, server and client](https://valram@bitbucket.org/valram/django_blogs_service.git)

* [Django, DRF, jQuery](https://valram@bitbucket.org/valram/django_blog)

* [Beautifulsoup4, requests, threading](https://github.com/romabiker/25_cinemas_site)

* [ simple_crawler_email_extractor ](https://bitbucket.org/valram/simple_crawler_email_extractor)

* [pyTelegramBotAPI](https://github.com/romabiker/28_pizza_bot)

* [Selenium, unittest](https://github.com/romabiker/31_atomicboard)

* [Flask](https://github.com/romabiker/24_telegraph)

* [Javascript](https://github.com/romabiker/34_timemachine)

* [Bootstrap](https://github.com/romabiker/22_proto_markup)
